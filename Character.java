//Vincent Ou, Joey Zeng, Anna Qu
//pd 9
//2013-11-18
//HW#26

public abstract class Character {
    protected String name;
    protected int health;
    protected int strength;
    protected int defense;
    protected double attack;
  //  protected boolean isSpecialized;
    //protected boolean isBlocked = false;
    protected String job;
    
    public Character(){
	name = "Bob";
	health = 125;
	strength = 100;
	defense = 40;
	attack = .4;
    }
    public boolean isAlive(){
	return health > 0;
    }
    public int getDefense(){
	return defense;
    }
    public void lowerHP( int x){
	health = health - x;
    }
    public int attack (Character name){
	int damage = 0;
	damage = (int)((strength * attack) - name.getDefense());
	if (damage < 0){
	    damage = 0;
	}
	name.lowerHP(damage);
	return damage;
    }
    public void specialize(){
    	reset();
    	/*
    	if (isBlocked){
    		defense /= 3;
    		isBlocked = false;
    	}
    	if (!isSpecialized){
    		attack *=2;
    		defense /=2;
    	}
    	isSpecialized = true;
    	*/
    	attack *=2;
    	defense /=2;
    }
    public void normalize(){
    	reset();
    	/*
    	if (isBlocked){
    		defense /= 3;
    		isBlocked = false;
    	}
	if(isSpecialized){
		attack /=2;
		defense *=2;
	}
	isSpecialized = false;
	*/
    }
	
    public String getName(){
	return name;
    }
    public void block(){
    	reset();
    	/*
    	if (!isBlocked){
    		int x = (int)(2 * Math.random());
    		if (x == 0){
    			defense *= 3;
    			strength -= 90;
    			isBlocked = true;
    		}
    		
    	}
    	*/
    	defense *= 3;
    	strength /= 5;
    }
       
    public String stats() {
	return "Name: " + name + "\nHealth: " + health + 
	    "\nStrength: " + strength + "\nDefense: " + defense + 
	    "\nAttack rating: " + attack;
    }
    public String getJob(){
    	return job;
    }
    abstract String about();
    abstract void reset();


}
